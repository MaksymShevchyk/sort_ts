import { Sorter } from './Sorter';
import { NumbersColletion } from './NumbersColletion';
import { CharactersColection } from './CharactersColection';
import { LinkedList } from './LinkedList';

//sort number
const numbersColletion = new NumbersColletion([-10, -2, 3, 4, -1]);
numbersColletion.sort();
console.log(numbersColletion.data)

//sort string
const charactersColection = new CharactersColection('awzt');
charactersColection.sort();
console.log(charactersColection.data)

//sort linked list
const linkedList = new LinkedList();
linkedList.add(500);
linkedList.add(-10);
linkedList.add(-3);
linkedList.add(4);

linkedList.sort();
linkedList.print();
